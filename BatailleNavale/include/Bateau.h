#ifndef BATEAU_H
#define BATEAU_H
#include <vector>
#include <iostream>
using namespace std;

class Bateau
{
    public:
        Bateau();
        Bateau(vector <int> pos_x, vector <int> pos_y,int type,int vie,bool horizontal);
        virtual ~Bateau();

        void setChoisi(bool choisi);
        void setPos_x(int posx);
        void setPos_y(int posy);
        void mouvementX(int x);
        void mouvementY(int y);
        int rotation();
        void setType(int type);
        void setVie(int nbVie);
        void setHorizontal(bool horizontal);
        void setTouche(bool touche);
        void setTouchePos(int x, int y);

        vector<int> getTouche_x();
        vector<int> getTouche_y();
        vector <int> getPos_x() const;
        vector <int> getPos_y() const;
        int getPos_x2(int i) const;
        int getPos_y2(int i) const;
        int getType() const;
        int getVie() const;
        bool getChoisi() const;
        bool getHorizontal() const;

        bool getTouche() const;

    protected:

    private:
        vector<int> m_touche_x;
        vector<int> m_touche_y;
        vector <int> m_pos_x;
        vector <int> m_pos_y;
        int m_type;
        int m_vie;
        bool m_touche;
        bool m_horizontal;
        bool m_choisi;
};

#endif // BATEAU_H
